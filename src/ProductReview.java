/*
  @author Amina
 */
public class ProductReview{
    private int rating;
    private String review;

    public ProductReview(int rating, String review) {
        this.rating = rating;
        this.review = review;
    }

    //region Getters and Setters
    public String getReview() {
        return review;
    }

    public int getRating(){
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Product review: " + rating + "; " +
                " , " + review;
    }
}
