/*
class for manage every thing
 */
import java.util.*;

public class ControlSystem
{
    private ArrayList<Catalog> catalogs;
    private HashMap<String, Member> members;
    private ArrayList<CreditCard> creditCards = new ArrayList<>();
    private ArrayList<String> electronicBankTransfer = new ArrayList<>();
    private ArrayList<Shipment> shipments = new ArrayList<>();
    private Scanner in;

    public ControlSystem()
    {
        catalogs = new ArrayList<>();
        members = new HashMap<>();
        in = new Scanner(System.in);
        addElectronicBankTransfar();
        catalogs.add(new Catalog("Electronic"));
        catalogs.add(new Catalog("Car"));
    }

    public void adminMenu()
    {
        System.out.println("1.To Block User\n2.To add new Product Category\n3.To modify product category\n4.To Quit");
    }

    public void guestMenu()
    {
        Guest guest = new Guest();
        int choice = -1;
        while(choice != 4)
        {
            System.out.println("1. Register\n2. Login\n3. Select product\n4. Exit");
            choice = CheckInput.checkInt(1, 4);
            switch (choice)
            {
                case 1:
                    registerAccount();
                    break;
                case 2:
                    login();
                    break;
                case 3:
                    selectProduct(guest);
                    break;
            }
        }
    }

    public void showMembers()
    {
        for (var key:members.keySet())
        {
            System.out.println(key + " " + members.get(key).toString());
        }
    }

    public void showProductsAndCategories(Catalog catalog)
    {
        var categories = catalog.getProductCategories();
        for (var key: categories.keySet())
        {
            System.out.println(key + categories.get(key));
        }
    }

    public void adminPanel(Admin admin)
    {
        int choice = -1;
        while(choice!=4){
            adminMenu();
            choice = CheckInput.checkInt(1,4);
            switch (choice)
            {
                case 1:
                    System.out.println("All members:");
                    showMembers();
                    System.out.println("Enter the email of the user you want to block.");
                    String mail = in.next();
                    if(members.containsKey(mail))
                    {
                        admin.blockUser(members.get(mail).account);
                    }
                    else
                    {
                        System.out.println("E-mail is wrong.");
                    }
                    break;
                case 2:
                    Catalog catalogForAdd = catalogChoice();
                    showProductsAndCategories(catalogForAdd);
                    System.out.println("Insert the new Category:");
                    String category = in.next();
                    if (catalogForAdd.getProductCategories().containsKey(category))
                    {
                        System.out.println("This category already exists.");
                    }
                    else
                    {
                        if(admin.addNewProductCategory(category, catalogForAdd.getProductCategories()))
                        {
                            System.out.println("Successfully added.");
                        }
                        else
                        {
                            System.out.println("Something is wrong");
                        }
                    }
                    break;
                case 3:
                    Catalog catalogForModify = catalogChoice();
                    showProductsAndCategories(catalogForModify);
                    System.out.println("Insert the old category:");
                    String oldCategory = in.next();
                    System.out.println("Insert the new category:");
                    String newCategory = in.next();
                    if (catalogForModify.getProductCategories().containsKey(newCategory))
                    {
                        System.out.println("New Category that you inserted already exists.");
                    }
                    else
                    {
                        if (admin.modifyProductCategory(oldCategory, newCategory, catalogForModify.getProductCategories()))
                        {
                            System.out.println("Successfully modified");
                        }
                        else
                        {
                            System.out.println("Wrong category.");
                        }
                    }
                    break;
                case 4:
                    logout();
                    break;
            }
        }
    }

    public Catalog catalogChoice()
    {
        System.out.println("Choose a catalog:");
        for (int i = 0; i < catalogs.size(); i++) {
            System.out.println(i+1 + "." + catalogs.get(i).getName());
        }
        return catalogs.get(CheckInput.checkInt(1, catalogs.size()) - 1);
    }

    public void memberMenu(Member user)
    {
        int choice = -1;
        while(choice != 11)
        {
            System.out.println("1. Select Product\n2. Show shopping cart\n3. Modify shopping cart\n4. Buy items of " +
                    "shopping cart\n5. Add Product\n6. Add review to product\n7. Show my email notifications\n8. " +
                    "Show my SMS notifications\n9. Add credit card\n10. Add electronic bank\n11. Logout");
            choice = CheckInput.checkInt(1, 11);
            switch (choice)
            {
                case 1:
                    selectProduct(user);
                    break;
                case 2:
                    System.out.println(user.getShoppingCart().toString());
                    break;
                case 3:
                    System.out.println("1. Remove item\n2. Add item\n3. Update quantity");
                    int modifyChoice = CheckInput.checkInt(1, 3);
                    switch (modifyChoice)
                    {
                        case 1:
                            if(user.getShoppingCart() != null && user.getShoppingCart().getItems().size() > 0)
                            {
                                System.out.println("Choose item");
                                System.out.println(user.getShoppingCart().toString());
                                int index = CheckInput.checkInt(1, user.getShoppingCart().getItems().size());
                                user.getShoppingCart().removeItem(user.getShoppingCart().getItems().get(--index));
                            }
                            else
                            {
                                System.out.println("Your shopping cart is empty");
                            }
                            break;
                        case 2:
                            selectProduct(user);
                            break;
                        case 3:
                            if(user.getShoppingCart() != null && user.getShoppingCart().getItems().size() > 0)
                            {
                                System.out.println("Choose item");
                                System.out.println(user.getShoppingCart().toString());
                                int index = CheckInput.checkInt(1, user.getShoppingCart().getItems().size());
                                System.out.println("Enter new quantity");
                                int quantity = in.nextInt();
                                user.getShoppingCart().getItems().get(--index).updateQuantity(quantity);
                            }
                            else
                            {
                                System.out.println("Your shopping cart is empty");
                            }
                            break;
                    }
                    break;
                case 4:
                    buy(user);
                    break;
                case 5:
                    for(int i = 0; i < catalogs.size(); ++i)
                    {
                        System.out.println((i + 1) + ". " + catalogs.get(i).getName());
                    }
                    System.out.println("Choose one catalog");
                    int index = CheckInput.checkInt(1, catalogs.size());
                    Product newProduct = addProduct();
                    catalogs.get(--index).addProduct(newProduct);
                    break;
                case 6:
                    for(int i = 0; i < catalogs.size(); ++i)
                    {
                        System.out.println((i + 1) + ". " + catalogs.get(i).getName());
                    }
                    System.out.println("Choose one catalog");
                    index = CheckInput.checkInt(1, catalogs.size());
                    System.out.println("If you want to search product by name or category enter 1 or 2 for choose product of list");
                    int variant = CheckInput.checkInt(1, 2);
                    Product product;
                    if(variant == 1)
                    {
                        product = searchProduct(catalogs.get(--index));
                    }
                    else
                    {
                        product = showCatalogs(catalogs.get(--index));
                    }
                    System.out.println("Enter your rating");
                    int rating = in.nextInt();
                    System.out.println("Enter your review");
                    String review = in.next();
                    product.addProductReview(new ProductReview(rating, review));
                    break;
                case 7:
                    NotificationSystem.showEmailNotifications(user.account.getEmail());
                    break;
                case 8:
                    NotificationSystem.showSMSNotifications(user.account.getPhone());
                    break;
                case 9:
                    addCreditCard(user);
                    break;
                case 10:
                    electronicBankTransfer(user);
                    break;
                case 11:
                    logout();
                    break;
            }
        }
    }

    //Method for select new product from catalog
    public void selectProduct(Customer customer)
    {
        for (int i = 0; i < catalogs.size(); ++i)
        {
            System.out.println((i + 1) + ". " + catalogs.get(i).getName());
        }
        System.out.println("Enter number of menu");
        int index = CheckInput.checkInt(1, catalogs.size());
        Catalog chosen = catalogs.get(--index);
        System.out.println("If you want to search product by name or category enter 1 or 2 for choose product of list");
        int variant = CheckInput.checkInt(1, 2);
        Product product;
        if(variant == 1)
        {
            product = searchProduct(chosen);
        }
        else
        {
            product = showCatalogs(chosen);
        }
        if(product != null)
        {
            System.out.println(product.toString());
            System.out.println(product.lookReviews());
            System.out.println("Would you like buy this product?\nEnter \"yes\" or \"no\"");
            String want = CheckInput.checkAnswer();
            if(want.equals("yes"))
            {
                customer.getShoppingCart().addItem(new Item(product));
            }
        }
        System.out.println("Would you like to repeat?\nEnter \"yes\" or \"no\"");
        String answer = CheckInput.checkAnswer();
        if(answer.equals("yes"))
        {
            selectProduct(customer);
        }
    }

    public Product searchProduct(Catalog chosen)
    {
        System.out.println("If you want to search by name enter 1 or 2 for search by category");
        int type = CheckInput.checkInt(1, 2);

        if(type == 1)
        {
            System.out.println("Enter name");
            String name = in.next();
            if(chosen.hasProductWithName(name))
            {
                chosen.SearchProductsByName(name);
                System.out.println("Choose product by number");
                int number = CheckInput.checkInt(1, chosen.getProductsWithName(name).size());
                return chosen.getProductByName(name, --number);
            }
            System.out.println("Sorry we don't have product with this name");
            return null;
        }
        else
        {
            System.out.println("Enter category");
            String category = in.next();
            if(chosen.hasProductWithCategory(category))
            {
                chosen.SearchProductsByCategory(category);
                int number = CheckInput.checkInt(1, chosen.getProductsWithCategory(category).size());
                return chosen.getProductByCategory(category, --number);
            }
            System.out.println("Sorry we don't have product with this category");
            return null;
        }
    }

    public Product showCatalogs(Catalog chosen)
    {
        if(chosen.getProducts() == null || chosen.getProducts().size() == 0)
        {
            System.out.println("Sorry there are no products in this catalog");
            return null;
        }
        for (int i = 0; i < chosen.getProducts().size(); ++i)
        {
            System.out.println((i + 1) + ". " + chosen.getProducts().get(i).toString());
        }
        System.out.println("Enter number of menu");
        return chosen.getProducts().get(CheckInput.checkInt(1, chosen.getProducts().size()) - 1);
    }

    public void buy(Member member)
    {
        Order order = new Order();
        System.out.println("If you want to pay with Electronic bank enter 1 or 2 for pay with credit card");
        int choice = CheckInput.checkInt(1, 2);
        Payment payment;
        if(choice == 1)
        {
            payment = new ElectronicBankTransaction(member.account);
        }
        else
        {
            payment = new CreditCardTransaction(member.account);
        }
        if(order.sendForShipment(member.getShoppingCart(), payment))
        {
            Shipment shipment = new Shipment("By mail", new Date());
            shipments.add(shipment);
            order.setStatus(OrderStatus.Shipped);
            System.out.println("Success");
        }
        OrderLog log = new OrderLog(order.getStatus());
        log.check(member.account);
        order.getLogs().add(log);
    }

    public Product addProduct()
    {
        System.out.println("Enter product name");
        String name = in.next();
        System.out.println("Enter product description");
        String description = in.next();
        System.out.println("Enter product category");
        String category = in.next();
        System.out.println("Enter product price");
        double price = in.nextDouble();
        System.out.println("Enter product available count");
        int count = in.nextInt();
        ProductCategory productCategory = new ProductCategory(category, description);
        return new Product(name, description, price, count, productCategory);
    }

    public void addElectronicBankTransfar()
    {
        electronicBankTransfer.add("ATF");
        electronicBankTransfer.add("Kaspi");
        electronicBankTransfer.add("Halyk");
        electronicBankTransfer.add("Home");
        electronicBankTransfer.add("Alfa");
    }

    public boolean electronicBankTransfer(Member member)
    {
        System.out.println(" Enter your bank name ");
        String bankName = in.next();
        Account account = member.account;
        if(electronicBankTransfer.contains(bankName))
        {
            System.out.println(" Enter tour routing number ");
            String routingNumber = in.next();
            System.out.println(" Enter your account number");
            String accNumber = in.next();
            account.setTransfer(new ElectronicBankTransfer(bankName , routingNumber , accNumber));
            return true;
        }
        else
        {
            System.out.println("We do not serve such a bank");
        }
        return false;

    }

    public void addCreditCard(Member member)
    {
        System.out.println(" Add your card ");
        System.out.println(" Enter name of Card : ");
        String nameOfCard = in.next();
        System.out.println(" Enter your card Number ");
        String cardNumber = in.next();
        System.out.println(" Enter your code ");
        int code = in.nextInt();
        CreditCard creditCard = new CreditCard(nameOfCard ,cardNumber, code , 10000 );
        creditCards.add(creditCard);
        member.account.setCard(creditCard);
    }

    public void login()
    {
        System.out.println("Enter your email address");
        String email = in.next();
        System.out.println("Enter password");
        String password = in.next();
        if(members.containsKey(email))
        {
            if(members.get(email).account.getPassword().equals(password))
            {
                if (members.get(email).account.getStatus().equals(AccountStatus.Active))
                {
                    memberMenu(members.get(email));
                }
                else
                {
                    System.out.println("Your account is:" + " " + members.get(email).account.getStatus());
                }
            }
            else
            {
                System.out.println("Incorrect password");
            }
        }
        else if(email.equals("tima.orazkulov01@mail.ru") && password.equals("qwerty123"))
        {
            adminPanel(new Admin());
        }
        else
        {
            System.out.println("Member with this email not found");
        }
    }

    public void logout()
    {
        System.out.println("Good bye");
    }

    public void registerAccount()
    {

        System.out.println( " Account registration ");
        System.out.println( " Enter your user name : ");
        String userName = in.next();
        System.out.println(" Enter your password : ");
        String password = in.next();
        System.out.println(" Enter your name : ");
        String name1 = in.next();
        System.out.println(" Enter your email address : ");
        String email = in.next();
        System.out.println(" Enter your phone number : ");
        String number = in.next();
                if (members.containsKey(email))
                {
                    System.out.println("This account is registered");
                    System.out.println("Try again");
                    registerAccount();
                }
                else{
                    members.put(email , new Member(new Account(userName , password , name1 , email , number , AccountStatus.Active ,
                                                     new Address("0 " , " 1" , "2" , "3","4" ))));
                    memberMenu(members.get(email));
                }
    }

}
