public interface ISearch {
    void SearchProductsByName(String name);

    void SearchProductsByCategory(String category);
}
