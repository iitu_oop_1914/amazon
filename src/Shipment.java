/*
  @author Dimash
 */
import java.util.ArrayList;
import java.util.Date;

public class Shipment {
    private String shipmentMethod;
    private Date shipmentDate;
    private Date estimatedArrival;
    private ArrayList<ShipmentLog> logs;

    public Shipment(String shipmentMethod, Date estimatedArrival)
    {
        this.shipmentMethod = shipmentMethod;
        this.shipmentDate = new Date();
        this.estimatedArrival = estimatedArrival;
        logs = new ArrayList<>();
    }

    public Date getShipmentDate() {
        return shipmentDate;
    }

    public Date getEstimatedArrival() {
        return estimatedArrival;
    }

    public String getShipmentMethod() {
        return shipmentMethod;
    }

    public boolean addShipmentLog(ShipmentLog log)
    {
        return logs.add(log);
    }
}
