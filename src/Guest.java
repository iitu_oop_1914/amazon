import java.util.ArrayList;
import java.util.Scanner;

public class Guest extends Customer
{
    public Guest() {
        super();
    }

    public boolean registerAccount(Account account, ArrayList<Customer> customers)
    {
        if(account!=null) {
            Member member = new Member(account);
            customers.add(member);
            return true;
        }
        return false;
    }

    @Override
    public ShoppingCart getShoppingCart() {
        return getCart();
    }
}
