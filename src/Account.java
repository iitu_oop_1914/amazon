import java.util.ArrayList;
import  java.util.*;
public class Account {
    private String userName, password, name, email, phone;
    private AccountStatus status;
    private Address shippingAddress;
    private CreditCard card;
    private  ElectronicBankTransfer transfer;

    public Account(String userName,String password,String name,String email, String phone,AccountStatus status, Address shippingAddress) {
        this.userName = userName;
        this.password=password;
        this.name=name;
        this.email=email;
        this.phone=phone;
        this.status=status;
        this.shippingAddress=shippingAddress;
        card = null;
        transfer = null;
    }

    public String getUserName() {
        return userName;
    }
    public String getPassword() {
        return password;
    }
    public String getName() {
        return name;
    }
    public String getEmail() {
        return email;
    }
    public String getPhone() {
        return phone;
    }
    public AccountStatus getStatus() {
        return status;
    }
    public Address getShippingAddress()
    {
        return shippingAddress;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setStatus(AccountStatus status) {
        this.status = status;
    }
    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
    public CreditCard getCard() {
        return card;
    }
    public void setCard(CreditCard card) {
        this.card = card;
    }
    public ElectronicBankTransfer getTransfer() {
        return transfer;
    }
    public void setTransfer(ElectronicBankTransfer transfer) {
        this.transfer = transfer;
    }

    public boolean addProductReview(Product product,ProductReview productReview)
    {
        if(product != null) {
            product.addProductReview(productReview);
            return true;
        }
        return false;
    }

    public boolean addProduct(Product product, Catalog catalog) {
        return catalog.addProduct(product);
    }

    public String toString() {
        return "Account:" +
                "User Name: " + userName +
                ", Password: " + password +
                ", Name: " + name +
                ", Email: " + email +
                ", Phone: " + phone +
                ", Status: " + status +
                ", Shipping Address: " + shippingAddress;
    }
}

