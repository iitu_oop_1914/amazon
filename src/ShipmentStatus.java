public enum ShipmentStatus {
    Pending, Shipped, Delivered, OnHold
}
