import java.util.ArrayList;
import java.util.Map;

public class Admin{
    Account account;
    public Admin(){
        account = new Account("Tima","qwerty123",
                "Temirlan","tima.orazkulov01@mail.ru","+(7)7072052004",
                AccountStatus.Active,new Address("Manasa 45",
                "Almaty city", "Almaty city",
                "080002","Almaty"));
    }
    public boolean blockUser(Account account)
    {
        if(account.getStatus() !=null) {
            account.setStatus(AccountStatus.Blocked);
            return true;
        }
        return false;
    }

    public boolean addNewProductCategory(String newCategory, Map<String,ArrayList<Product>> map)
    {
        if(newCategory!=null){
            map.put(newCategory,new ArrayList<Product>());
            return true;
        }
        return false;
    }

    public boolean modifyProductCategory(String oldCategory,String newCategory, Map<String,ArrayList<Product>> map) {
        if(newCategory != null && map.containsKey(oldCategory)){
            map.put(newCategory, map.get(oldCategory));
            map.remove(oldCategory);
            return true;
        }
        return false;
    }
}
