/*
  @author Bayan
 */
import java.util.Date;

public class SMSNotification extends Notification {
    private String phone;
    private static int id = 0;
    public SMSNotification(String content,String phone) {
        super(++id, new Date(),content);
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean sendNotification()
    {
        return NotificationSystem.sendSMSNotification(this);
    }

}
