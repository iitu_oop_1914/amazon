public abstract class Customer
{
    protected ShoppingCart cart;

    public Customer()
    {
        cart = new ShoppingCart();
    }

    public abstract ShoppingCart getShoppingCart();

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }
}
