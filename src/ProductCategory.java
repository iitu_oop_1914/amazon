/*
  @author Amina
 */
public class ProductCategory
{
    private String name;
    private String description;

    public ProductCategory(String name, String description)
    {
        this.name = name;
        this.description = description;
    }

    //region Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    //endregion

    @Override
    public String toString() {
        return "ProductCategory:" +
                "Name: " + name +
                ", Description: " + description;
    }
}
