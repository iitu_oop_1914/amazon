import java.util.ArrayList;
import java.util.Scanner;
import  java.util.*;
public class CreditCard   {
    private String nameOfCard, cardNumber;
    private int code;
    private double money;


    public CreditCard(String nameOfCard, String cardNumber, int code, double money) {
        this.nameOfCard = nameOfCard;
        this.cardNumber = cardNumber;
        this.code = code;
        this.money = money;
    }

    public CreditCard() {
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getNameOfCard() {
        return nameOfCard;
    }

    public void setNameOfCart(String nameOfCard) {
        this.nameOfCard = nameOfCard;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }





    @Override
    public String toString()
    {
        return "Name of card : " + nameOfCard +
                ", Card number : " + cardNumber +
                ", Code : " + code  +
                ", Money " + money;

    }


}
