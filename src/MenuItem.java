public enum MenuItem
{
    ShowCatalog, Register, Login, Logout, AddCreditCart, AddShippingAddress, UpdateAccount, CancelMembership,
    ModifyProduct, AddProduct, UpdateCatalog, AddItemToShoppingCard, UpdateShoppingCard, SearchProduct
}
