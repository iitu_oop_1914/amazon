import java.util.ArrayList;
import  java.util.*;
public class ElectronicBankTransfer {
    String bankName;
    String routingNumber;
    String accountNumber;

    public ElectronicBankTransfer(String bankName, String routingNumber, String accountNumber) {
        this.bankName = bankName;
        this.routingNumber = routingNumber;
        this.accountNumber = accountNumber;
    }

    public ElectronicBankTransfer() {
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String toString() {
        return "Bank name : " + bankName + " Roting Number:" + routingNumber + " Account Number:" + accountNumber;
    }

}

