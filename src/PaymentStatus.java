public enum PaymentStatus {
    Unpaid, Pending, Completed, Failed, Declined, Canceled, Abandoned, Setting, Settled, Refunded
}
