/*
  @author Abosh
 */
import java.util.*;
public class Catalog implements ISearch {
    private Date lastUpdated;
    private Map<String, ArrayList<Product>> productNames;
    private Map<String, ArrayList<Product>> productCategories;
    private ArrayList<Product> products;
    private String name;

    public Catalog(String name) {
        this.name = name;
        productNames = new HashMap<>();
        productCategories = new HashMap<>();
        lastUpdated = new Date();
        products = new ArrayList<>();
    }

    //region Getters and Setters
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Map<String, ArrayList<Product>> getProductNames() {
        return productNames;
    }

    public void setProductNames(Map<String, ArrayList<Product>> productNames) {
        this.productNames = productNames;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public Map<String, ArrayList<Product>> getProductCategories() {
        return productCategories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product getProductByName(String productName, int index) {
        return productNames.get(productName).get(index);
    }

    public Product getProductByCategory(String category, int index) {
        return productCategories.get(category).get(index);
    }
    //end region

    @Override
    public void SearchProductsByName(String productName) {
        if (productNames.containsKey(productName)) {
            for (int i = 0; i < productNames.get(productName).size(); ++i) {
                System.out.println((i + 1) + ". " + productNames.get(productName).get(i));
            }
        }
    }

    @Override
    public void SearchProductsByCategory(String category) {
        if (productCategories.containsKey(category)) {
            for (int i = 0; i < productCategories.get(category).size(); ++i) {
                System.out.println((i + 1) + ". " + productCategories.get(category).get(i).toString());
            }
        }
    }

    public boolean addProductToName(String productName, Product newProduct)//Add new product to catalog and update time
    {
        try //check added new product or not, if not return false else true
        {
            if (!productNames.containsKey(productName)) {
                productNames.put(productName, new ArrayList<>());
            }
            productNames.get(productName).add(newProduct);
            lastUpdated = new Date();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    // Add new product to catalog and update date when catalog last updated
    public boolean addProductToCategory(String category, Product newProduct) {
        try {
            if (!productCategories.containsKey(category)) {
                productCategories.put(category, new ArrayList<>());
            }
            productCategories.get(category).add(newProduct);
            lastUpdated = new Date();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean addProduct(Product product) {
        return addProductToCategory(product.getCategory().getName(), product) && addProductToName(product.getName(),
                product) && products.add(product);
    }

    public boolean hasProductWithName(String name) {
        return productNames.containsKey(name);
    }

    public boolean hasProductWithCategory(String category) { return productCategories.containsKey(category);}

    public ArrayList<Product> getProductsWithName(String name) {
        return productNames.get(name);
    }

    public ArrayList<Product> getProductsWithCategory(String category) {
        return productCategories.get(category);
    }

    @Override
    public String toString()
    {
        String res = "Name: " + name + "\n";
        for(int i = 0; i < products.size(); ++i)
        {
            res += "\t" + (i + 1) + ". " + products.get(i).toString() + "\n";
        }
        return res;
    }
}
