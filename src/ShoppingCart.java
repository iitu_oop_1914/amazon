/*
  @author Meiirkhan
 */
import java.util.ArrayList;

public class ShoppingCart
{
    ArrayList<Item> items;

    public ShoppingCart() {
        this.items = new ArrayList<>();
    }

    public boolean addItem(Item item)
    {
        return items.add(item);
    }

    public boolean removeItem(Item item)
    {
        return items.remove(item);
    }

    public ArrayList<Item> getItems()
    {
        return items;
    }


    public double getTotalPrice() {

        double TotalPrice = 0;
        for(int i=0;i<items.size();i++){
            TotalPrice+=items.get(i).getPrice()* items.get(i).getQuantity();
        }
        return TotalPrice;
    }
    @Override
    public String toString()
    {
        String res = "";
        for(int i = 0; i < items.size(); ++i)
        {
            res += "\t" + (i + 1) + "). " + items.get(i).toString() + "\n";
        }
        return res;
    }
}
