/*
  @author Abosh
 */
import java.util.*;

public class Order
{
    private String orderNumber;
    private OrderStatus status;
    private Date orderDate;
    private ArrayList<OrderLog> logs;
    private static long id;

    public Order()
    {
        ++id;
        orderNumber = "" + id;
        orderDate = new Date();
        logs = new ArrayList<>();
        status = OrderStatus.Unshipped;
    }

    public Order(String orderNumber, OrderStatus status)
    {
        this.orderNumber = orderNumber;
        this.status = status;
        this.orderDate = new Date();
        logs = new ArrayList<>();
    }

    //getters and setters region
    public String getOrderNumber() {
        return orderNumber;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public ArrayList<OrderLog> getLogs()
    {
        return logs;
    }

    public void setLogs(ArrayList<OrderLog> logs)
    {
        this.logs = logs;
    }

    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public void setStatus(OrderStatus status)
    {
        this.status = status;
    }

    public void setOrderDate(Date orderDate)
    {
        this.orderDate = orderDate;
    }
    //end region

    public boolean sendForShipment(ShoppingCart cart, Payment payment)//calculate total price and check payment status
    {
        return payment.processPayment(cart.getTotalPrice()) == PaymentStatus.Completed;//if payment was success return
        // true
    }
}
