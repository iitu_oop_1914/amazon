public class Member extends Customer {
    Account account;

    public Member(Account account) {
        super();
        this.account = account;
    }

    public boolean placeOrder()
    {
        return true;
    }

    @Override
    public ShoppingCart getShoppingCart() {
        return getCart();
    }
}
