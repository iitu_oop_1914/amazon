/*
  @author Meiirkhan
 */
public class Item
{
    private double price;
    private int quantity;

    public Item(double price, int quantity)
    {
        this.quantity = quantity;
        this.price = price;
    }

    public Item(Product product, int quantity)
    {
        this(product.getPrice(), quantity);
    }

    public Item(Product product)
    {
        this(product.getPrice(), 1);
    }

    public Item(double price)
    {
        this(price, 1);
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public boolean updateQuantity(int newQuantity)
    {
        if(newQuantity >= 0)
        {
            quantity = newQuantity;
            return true;
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "Item: Quantity: " + quantity + ", Price: " + price;
    }
}
