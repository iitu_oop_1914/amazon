/*
  @author Dimash
 */
import java.util.Date;

public class ShipmentLog {
    private ShipmentStatus status;
    private Date creationDate;

    public ShipmentLog(ShipmentStatus status, Date creationDate)
    {
        this.status = status;
        this.creationDate = creationDate;
    }

    public ShipmentStatus getStatus() {
        return status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    @Override
    public String toString() {
        return "Creation date: " + creationDate.toString() + ", Status: " + status;
    }
}
