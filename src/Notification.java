/*
  @author Bayan
 */
import java.time.format.DateTimeFormatter;
import java.util.Date;

public abstract class Notification {
    protected int notificationId;
    protected Date createdOn;
    protected String content;

    public abstract boolean sendNotification();

    public Notification(int notificationId, Date createdOn, String content) {
        this.notificationId = notificationId;
        this.createdOn = createdOn;
        this.content = content;
    }
}
