public class ElectronicBankTransaction extends Payment{
    Account acc;

    public ElectronicBankTransaction(Account acc) {
        this.acc = acc;
    }

    public void registrationOfTransfer(ElectronicBankTransfer electronicBankTransfer){
        acc.setTransfer(electronicBankTransfer);
    }

    @Override
    public PaymentStatus processPayment(double totalPrice) {
        if(acc.getTransfer() !=null){
            return PaymentStatus.Completed;
        }
        return PaymentStatus.Failed;
        }
    }


