import java.util.*;

public class NotificationSystem {
    private static HashMap<String, ArrayList<SMSNotification>> phoneNotifications = new HashMap<>();
    private static HashMap<String, ArrayList<EmailNotification>> emailNotifications = new HashMap<>();

    public static boolean sendSMSNotification(SMSNotification notification)
    {
        try
        {
            if(!phoneNotifications.containsKey(notification.getPhone()))
            {
                phoneNotifications.put(notification.getPhone(), new ArrayList<>());
            }
            phoneNotifications.get(notification.getPhone()).add(notification);
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    public static boolean sendEmailNotification(EmailNotification notification)
    {
        try
        {
            if(!emailNotifications.containsKey(notification.getEmail()))
            {
                emailNotifications.put(notification.getEmail(), new ArrayList<>());
            }
            emailNotifications.get(notification.getEmail()).add(notification);
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    public static void showEmailNotifications(String email)
    {
        if (!emailNotifications.containsKey(email))
        {
            System.out.println("You don't have any notification");
            return;
        }
        for(int i = 0; i < emailNotifications.get(email).size(); ++i)
        {
            System.out.println((i + 1) + ". " + emailNotifications.get(email).get(i).toString());
        }
    }

    public static void showSMSNotifications(String phone)
    {
        if (!phoneNotifications.containsKey(phone))
        {
            System.out.println("You don't have any notification");
            return;
        }
        for(int i = 0; i < phoneNotifications.get(phone).size(); ++i)
        {
            System.out.println((i + 1) + ". " + phoneNotifications.get(phone).get(i).toString());
        }
    }
}
