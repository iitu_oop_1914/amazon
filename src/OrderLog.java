/*
  @author Abosh
 */
import java.util.Date;

public class OrderLog
{
    private OrderStatus status;
    private Date creationDate;

    public OrderLog(OrderStatus status)
    {
        this.status = status;
        this.creationDate = new Date();
    }

    public OrderLog(OrderStatus status, Date creationDate)
    {
        this.status = status;
        this.creationDate = creationDate;
    }

    //Getters and Setters region
    public Date getCreationDate()
    {
        return creationDate;
    }

    public OrderStatus getStatus()
    {
        return status;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public void setStatus(OrderStatus status)
    {
        this.status = status;
    }
    //endregion

    public boolean check(Account account)
    {
        Notification sms = new SMSNotification(status.toString(), account.getPhone());
        Notification email = new EmailNotification(status.toString(), account.getEmail());
        return sms.sendNotification() || email.sendNotification();
    }
}
