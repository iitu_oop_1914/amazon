/*
  @author Bayan
 */
import java.util.Date;

public class EmailNotification extends Notification {
    private String email;
    private static int id;
    public EmailNotification(String content,String email) {
        super(++id, new Date(), content);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean sendNotification(){
        return NotificationSystem.sendEmailNotification(this);
    }
}
