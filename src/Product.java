import java.util.ArrayList;

/*
  @author Amina
 */
public class Product {
    private String name;
    private String description;
    private double price;
    private int availableItemCount;
    private ProductCategory category;
    private ArrayList<ProductReview> productReviews;

    public Product(String name, String description, double price, int availableItemCount, ProductCategory category) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.availableItemCount = availableItemCount;
        this.category = category;
    }

    //region Getters and Setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getAvailableItemCount() {
        return availableItemCount;
    }
    public void setAvailableItemCount(int availableItemCount) {
        this.availableItemCount = availableItemCount;
    }
    public ProductCategory getCategory() {
        return category;
    }
    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public int getAvailableCount() {
        return availableItemCount;
    }

    public ArrayList<ProductReview> getProductReviews() {
        return productReviews;
    }

    public void setProductReviews(ArrayList<ProductReview> productReviews) {
        this.productReviews = productReviews;
    }

    public String lookReviews() {
        String res = "";
        for(ProductReview item : getProductReviews()) {
            res += item.toString();
        }
        return res;
    }

    public boolean addProductReview(ProductReview review) {
        return productReviews.add(review);
    }

    @Override
    public String toString() {
        return "Product: Name: " + name + ", Price: " + price + ", Description: " + description;
    }
}
