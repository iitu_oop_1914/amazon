  public abstract class Payment {
private PaymentStatus status;
private double amount;


     public PaymentStatus getStatus() { return status; }

     public void setStatus(PaymentStatus status) {
         this.status = status;
     }

     public double getAmount() {
         return amount;
     }

     public void setAmount(double amount) {
         this.amount = amount;
     }



      public abstract PaymentStatus processPayment(double totalPrice);
 }
