public class CreditCardTransaction extends Payment {
    Account acc;

    public CreditCardTransaction(Account account) {
        acc = account;
    }

    public void registrationOfCard(CreditCard creditCard){
        acc.setCard(creditCard);
    }

    @Override
    public PaymentStatus processPayment(double totalPrice) {
      if(acc.getCard() == null){
          System.out.println("Card is not added.");
          return PaymentStatus.Failed;
      }
        if(acc.getCard().getMoney() >= totalPrice)
      {
          double money =  acc.getCard().getMoney() - totalPrice;
          acc.getCard().setMoney(money);
          return PaymentStatus.Completed;
      }
      return PaymentStatus.Failed;
    }
}
