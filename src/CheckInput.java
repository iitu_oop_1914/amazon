import java.util.Scanner;

public class CheckInput
{
    private static Scanner in = new Scanner(System.in);

    public static int checkInt(int left, int right)//return int in the range
    {
        try
        {
            int ans = in.nextInt();
            if(!(left <= ans && ans <= right))
            {
                System.out.println("Please enter correct data");
                return checkInt(left, right);
            }
            return ans;
        }
        catch (Exception e)
        {
            System.out.println("Please enter correct data");
            return checkInt(left, right);
        }
    }

    public static String checkAnswer()//return string if it is equal to "yes" or "no"
    {
        String ans = in.next().toLowerCase();
        while(!ans.equals("yes") && !ans.equals("no"))
        {
            System.out.println("Please enter correct data");
            ans = in.next().toLowerCase();
        }
        return ans;
    }
}
